﻿using System.Collections.Generic;
using BillSharer.Core.Models;

namespace BillSharer.Core.Repositories
{
    public interface IUserAccountRepository
    {
        IEnumerable<UsernamePasswordModel> ReturnUsernamePasswordCombinations();
    }
}

