﻿using System;
using System.Collections.Generic;
using System.Linq;
using BillSharer.Core.DataHelper;
using BillSharer.Core.Models;

namespace BillSharer.Core.Repositories
{
    public class UserAccountRepository : IUserAccountRepository
    {
        List<UsernamePasswordModel> _usernamesAndPasswords;

        readonly IDatabase _database;

        public UserAccountRepository(IDatabase database)
        {
            _database = database;
            _database.DropTable<UsernamePasswordModel>(); //drop the table for testing purposes
            _database.CreateTable<UsernamePasswordModel>();
            InitializeRepository();
        }

        void InitializeRepository()
        {
            var firstAccount = CreateAccount("1", "1");

            //Check to see if account is not in the db
            if (!_database.SelectTable<UsernamePasswordModel>().Contains(_database.Find<UsernamePasswordModel>(a => a.UserId == firstAccount.UserId)))
            {
                _database.InsertIntoTable(firstAccount);
            }

            _usernamesAndPasswords = _database.SelectTable<UsernamePasswordModel>();
        }

        UsernamePasswordModel CreateAccount(string username, string password)
        {
            var usernameAndPassword = new UsernamePasswordModel
            {
                Username = username,
                Password = password,
                UserId = Guid.NewGuid()
            };

            return usernameAndPassword;
        }

        public IEnumerable<UsernamePasswordModel> ReturnUsernamePasswordCombinations()
        {
            return _usernamesAndPasswords.AsEnumerable();
        }
    }
}
