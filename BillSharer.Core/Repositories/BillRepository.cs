using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using BillSharer.Core.DataHelper;
using BillSharer.Core.Models;

namespace BillSharer.Core.Repositories
{
    public class BillRepository : IBillRepository
    {
        List<BillModel> _billList;

        readonly IDatabase _database;

        public BillRepository(IDatabase database)
        {
            _database = database;
            CreateDatabase();
            InitializeRepository();
        }

        public IEnumerable<BillModel> ReturnAllBills()
        {
            return _billList.AsEnumerable();
        }

        void InitializeRepository()
        {
            _billList = _database.SelectTable<BillModel>();
        }

        public bool CreateDatabase()
        {
            return _database.CreateTable<BillModel>();
        }

        public bool DeleteFromTable<BillModel>(Guid billId)
        {
            return _database.DeleteFromTable<BillModel>(billId);
        }

        public bool InsertIntoTable(BillModel billModel)
        {
            return _database.InsertIntoTable(billModel);
        }

        public List<BillModel> SelectTable()
        {
            return _database.SelectTable<BillModel>();
        }

        public T Find<T>(Expression<Func<T, bool>> predicate) where T : class, new()
        {
            return _database.Find(predicate);
        }

        public bool DropTable()
        {
            return _database.DropTable<BillRepository>();
        }

        BillModel CreateBill(string name, string description, DateTime due)
        {
            var bill = new BillModel
            {
                Name = name,
                Description = description,
                Due = due,
                Id = Guid.Parse("1")
            };

            return bill;
        }
    }
}
