﻿using System.Threading.Tasks;
using BillSharer.Core.Services;
using MvvmCross.Logging;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using MvvmMarketplaceApp.Core;

namespace BillSharer.Core.ViewModels
{
    public class LoginViewModel : MvxNavigationViewModel
    {
        string _username;
        string _password;

        readonly IMvxNavigationService _mvxNavigationService;
        readonly ILoginService _loginService;

        public LoginViewModel(IMvxLogProvider logProvider, IMvxNavigationService navigationService, ILoginService loginService)
            : base(logProvider, navigationService)
        {
            _mvxNavigationService = navigationService;
            _loginService = loginService;
        }

        public override async Task Initialize()
        {
            await base.Initialize();
        }

        public string Username
        {
            get { return _username; }
            set
            {
                _username = value;
                RaisePropertyChanged(() => Username);
            }
        }

        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                RaisePropertyChanged(() => Password);
            }
        }

        public LoginStatus LoginWithUsernamePassword()
        {
            return _loginService.LoginWithUsernamePassword(_username, _password);
        }

        public async Task SignIn()
        {
            if (LoginWithUsernamePassword() == LoginStatus.Successful)
            {
                await _mvxNavigationService.Navigate<LandingViewModel>();
            }
            else if (LoginWithUsernamePassword() == LoginStatus.Failed)
            {
                //Alert here 
            }
            else
            {
                //Different alert here
            }
        }
    }
}
