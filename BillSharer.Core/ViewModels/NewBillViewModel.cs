﻿using System;
using BillSharer.Core.Models;
using BillSharer.Core.Services;
using MvvmCross.Logging;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;

namespace BillSharer.Core.ViewModels
{
    public class NewBillViewModel : MvxNavigationViewModel
    {
        string _name;
        string _description;

        DateTime _due;

        readonly IBillService _billService;

        public NewBillViewModel(IMvxLogProvider logProvider, IMvxNavigationService navigationService, IBillService billService)
            : base(logProvider, navigationService)
        {
            _billService = billService;
        }

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                RaisePropertyChanged(() => Name);
            }
        }

        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                RaisePropertyChanged(() => Description);
            }
        }

        public DateTime Due
        {
            get { return _due; }
            set
            {
                _due = value;
                RaisePropertyChanged(() => Due);
            }
        }

        public void AddBill()
        {
            _billService.InsertIntoTable(
            new BillModel
            {
                Name = Name,
                Description = Description,
                Due = Due,
                Id = Guid.NewGuid()
            });
        }
    }
}
