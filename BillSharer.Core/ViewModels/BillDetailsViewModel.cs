﻿using System;
using System.Threading.Tasks;
using BillSharer.Core.Models;
using BillSharer.Core.Services;
using MvvmCross.Logging;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;

namespace BillSharer.Core.ViewModels
{
    public class BillDetailsViewModel : MvxNavigationViewModel<Guid>
    { 
        BillModel _billmodel;

        readonly IMvxNavigationService _navigationService;
        readonly IBillService _billService;

        public BillDetailsViewModel(IMvxLogProvider logProvider, IMvxNavigationService navigationService, IBillService billService) 
            : base(logProvider, navigationService)
        {
            _navigationService = navigationService;
            _billService = billService;
        }

        public override void Prepare(Guid parameter)
        {
            _billmodel = _billService.Find<BillModel>(b => b.Id == parameter);
        }

        public string Name
        {
            get { return _billmodel.Name; }
            private set
            {
                _billmodel.Name = value;
                RaisePropertyChanged(() => Name);
            }
        }

        public string Description
        {
            get { return _billmodel.Description; }
            set
            {
                _billmodel.Description = value;
                RaisePropertyChanged(() => Description);
            }
        }

        public DateTime Due
        {
            get { return _billmodel.Due; }
            set
            {
                _billmodel.Due = value;
                RaisePropertyChanged(() => Due);
            }
        }

        public void DeleteCurrentBill()
        {
            _billService.DeleteFromTable(_billmodel.Id);
        }

        public async Task NavigateToManageBills()
        {
            await _navigationService.Navigate<ManageBillsViewModel>();
        }
    }
}
