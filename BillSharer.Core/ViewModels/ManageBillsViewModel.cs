﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using BillSharer.Core.Models;
using BillSharer.Core.Services;
using MvvmCross.Logging;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;

namespace BillSharer.Core.ViewModels
{
    public class ManageBillsViewModel : MvxNavigationViewModel
    {
        ObservableCollection<BillModel> _bills;

        readonly IBillService _billService;
        readonly IMvxNavigationService _navigationService;

        public ManageBillsViewModel(IMvxLogProvider logProvider, IMvxNavigationService navigationService, IBillService billService)
            : base(logProvider, navigationService)
        {
            _billService = billService;
            _navigationService = navigationService;
            _bills = new ObservableCollection<BillModel>();

            FillBills();
        }

        public override async Task Initialize()
        {
            FillBills();
        }

        public ObservableCollection<BillModel> Bills
        {
            get { return _bills; }
            set
            {
                _bills = value;
                RaisePropertyChanged(() => Bills);
            }
        }


        public void NavigateToBillDetails(int billIndex)
        {
            var jobId = _bills[billIndex].Id;

            _navigationService.Navigate<BillDetailsViewModel, Guid>(jobId);
        }

        void FillBills()
        {
            Bills = new ObservableCollection<BillModel>(_billService.SelectTable());
        }
    }
}
