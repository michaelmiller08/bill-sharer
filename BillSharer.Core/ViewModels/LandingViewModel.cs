using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BillSharer.Core.Models;
using BillSharer.Core.Services;
using MvvmCross.Logging;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;

namespace BillSharer.Core.ViewModels
{
    public class LandingViewModel : MvxNavigationViewModel
    {
        List<BillModel> _bills;

        readonly IBillService _billService;
        readonly IMvxNavigationService _navigationService;

        public LandingViewModel(IMvxLogProvider logProvider, IMvxNavigationService navigationService, IBillService billService)
         : base(logProvider, navigationService)
        {
            _billService = billService;
            _navigationService = navigationService;
        }

        public override async Task Initialize()
        {
            await base.Initialize();

            RefreshBills();
        }

        public async Task NavigateToNewBill()
        {
            await _navigationService.Navigate<NewBillViewModel>();
        }

        public async Task NavigateToManageBills()
        {
            await _navigationService.Navigate<ManageBillsViewModel>();
        }

        public List<BillModel> Bills
        {
            get { return _bills; }
            set
            {
                _bills = value;
                RaisePropertyChanged(() => Bills);
            }
        }

        public void RefreshBills()
        {
            Bills = _billService.SelectTable();

            foreach (var bill in Bills)
            {
                if (bill.Due.Date.CompareTo(DateTime.UtcNow) < 0)
                {
                    bill.IsOverdue = true;
                }
            }
        }
    }
}
