﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using BillSharer.Core.Models;

namespace BillSharer.Core.Services
{
    public interface IBillService
    {
        bool CreateDatabase();

        bool DeleteFromTable(Guid billId);

        bool InsertIntoTable(BillModel billModel);

        List<BillModel> SelectTable();

        T Find<T>(Expression<Func<T, bool>> predicate) where T : class, new();

        bool DropTable();
    }
}
