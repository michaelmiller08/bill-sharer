﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using BillSharer.Core.Models;
using BillSharer.Core.Repositories;

namespace BillSharer.Core.Services
{
    public class BillService : IBillService
    {
        readonly IBillRepository _billRepository;

        public BillService(IBillRepository billRepository)
        {
            _billRepository = billRepository;
        }

        public bool CreateDatabase()
        {
            return _billRepository.CreateDatabase();
        }

        public bool DeleteFromTable(Guid billId)
        {
            return _billRepository.DeleteFromTable<BillModel>(billId);
        }

        public bool DropTable()
        {
            return _billRepository.DropTable();
        }

        public bool InsertIntoTable(BillModel billModel)
        {
            return _billRepository.InsertIntoTable(billModel);
        }

        public T Find<T>(Expression<Func<T, bool>> predicate) where T : class, new()
        {
            return _billRepository.Find(predicate);
        }

        public List<BillModel> SelectTable()
        {
            return _billRepository.SelectTable();
        }
    }
}
