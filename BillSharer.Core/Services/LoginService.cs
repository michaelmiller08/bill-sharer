﻿using System;
using System.Linq;
using BillSharer.Core.Repositories;
using MvvmMarketplaceApp.Core;

namespace BillSharer.Core.Services
{
    public class LoginService : ILoginService
    {
        readonly IUserAccountRepository _userAccountRepository;

        public LoginService(IUserAccountRepository userAccountRepository)
        {
            _userAccountRepository = userAccountRepository;
        }

        public LoginStatus LoginWithUsernamePassword(string username, string password)
        {
            var status = LoginStatus.Failed;

            try
            {
                if (_userAccountRepository.ReturnUsernamePasswordCombinations().First(p => p.Username == username).Password == password)
                {
                    status = LoginStatus.Successful;
                }
            }
            catch (InvalidOperationException)
            {
                //Catch if the details are not found
                //ToDo: show alert 
            }

            return status;
        }
    }
}
