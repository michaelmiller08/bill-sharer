﻿using MvvmMarketplaceApp.Core;

namespace BillSharer.Core.Services
{
    public interface ILoginService
    {
        LoginStatus LoginWithUsernamePassword(string username, string password);
    }
}
