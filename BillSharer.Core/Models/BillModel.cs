﻿using System;
using SQLite;

namespace BillSharer.Core.Models
{
    public class BillModel
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime Due { get; set; }

        public bool IsOverdue { get; set; }

        [PrimaryKey]
        public Guid Id { get; set; }
    }
}
