﻿using System;

namespace BillSharer.Core.Models
{
    public class UsernamePasswordModel
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public Guid UserId { get; set; }
    }
}
