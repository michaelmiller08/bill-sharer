﻿using System;
using System.Collections.Generic;
using System.IO;
using BillSharer.Core.Models;
using SQLite;

namespace BillSharer.Core.DataHelper
{
    public class UserAccountDatabase : IUserAccountDatabase
    {
        string _folderToSaveTo = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
        string _databaseName = "Creds.db";
        
        public bool CreateDatabase()
        {
            var success = false;

            try
            {
                using (var connection = new SQLiteConnection(Path.Combine(_folderToSaveTo, _databaseName)))
                {
                    connection.CreateTable<UsernamePasswordModel>();
                    success = true;
                }
            }
            catch (SQLiteException)
            {
                //Log ex
            }

            return success;
        }

        public bool DeleteFromTable(UsernamePasswordModel usernamePasswordModel)
        {
            var success = false;

            try
            {
                using (var connection = new SQLiteConnection(Path.Combine(_folderToSaveTo, _databaseName)))
                {
                    connection.Delete(usernamePasswordModel);
                    success = true;
                }
            }
            catch (SQLiteException)
            {
                //Log ex
            }

            return success;
        }

        public bool InsertIntoTable(UsernamePasswordModel usernamePasswordModel)
        {
            var success = false;

            try
            {
                using (var connection = new SQLiteConnection(Path.Combine(_folderToSaveTo, _databaseName)))
                {
                    connection.Insert(usernamePasswordModel);
                    success = true;
                }
            }
            catch (SQLiteException)
            {
                //Log ex
            }

            return success;
        }

        public List<UsernamePasswordModel> SelectTable()
        {
            List<UsernamePasswordModel> usernamePasswordModelList = null;

            try
            {
                using (var connection = new SQLiteConnection(Path.Combine(_folderToSaveTo, _databaseName)))
                {
                    usernamePasswordModelList =  connection.Table<UsernamePasswordModel>().ToList();
                }
            }
            catch (SQLiteException)
            {
                //Log ex
            }

            return usernamePasswordModelList;
        }

        public bool SelectQuery(Guid userId)
        {
            var success = false;

            try
            {
                using (var connection = new SQLiteConnection(Path.Combine(_folderToSaveTo, _databaseName)))
                {
                    connection.Query<UsernamePasswordModel>($"SELECT * FROM {_databaseName} Where UserId=?", userId);
                    success = true;
                }
            }
            catch (SQLiteException)
            {
                //Log ex
            }

            return success;
        }

        public bool DropTable()
        {
            var success = false;

            try
            {
                using (var connection = new SQLiteConnection(Path.Combine(_folderToSaveTo, _databaseName)))
                {
                    connection.DropTable<UsernamePasswordModel>();
                    success = true;
                }
            }
            catch (SQLiteException)
            {
                //Log ex
            }

            return success;
        }
    }
}
