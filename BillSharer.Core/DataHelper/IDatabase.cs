﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace BillSharer.Core.DataHelper
{
    public interface IDatabase
    {
        bool CreateTable<T>();

        bool DeleteFromTable<T>(Guid primaryKey);

        bool InsertIntoTable<T>(T model);

        List<T> SelectTable<T>() where T : class, new();

        T Find<T>(Expression<Func<T, bool>> predicate) where T : class, new();

        bool DropTable<T>();
    }
}
