using System;
using System.Collections.Generic;
using System.IO;
using System.Linq.Expressions;
using SQLite;

namespace BillSharer.Core.DataHelper
{
    public class Database : IDatabase
    {
        string _folderToSaveTo = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
        string _databaseName = "Sharer.db";

        public bool CreateTable<T>()
        {
            var success = false;

            try
            {
                using (var connection = new SQLiteConnection(Path.Combine(_folderToSaveTo, _databaseName)))
                {
                    connection.CreateTable<T>();
                    success = true;
                }
            }
            catch (SQLiteException)
            {
                //Log ex
            }

            return success;
        }

        public bool DeleteFromTable<T>(Guid primaryKey)
        {
            var success = false;

            try
            {
                using (var connection = new SQLiteConnection(Path.Combine(_folderToSaveTo, _databaseName)))
                {
                    connection.Delete<T>(primaryKey);
                    success = true;
                }
            }
            catch (SQLiteException)
            {
                //Log ex
            }

            return success;
        }

        public bool InsertIntoTable<T>(T model)
        {
            var success = false;

            try
            {
                using (var connection = new SQLiteConnection(Path.Combine(_folderToSaveTo, _databaseName)))
                {
                    connection.Insert(model);
                    success = true;
                }
            }
            catch (SQLiteException)
            {
                //Log ex
            }

            return success;
        }

        public List<T> SelectTable<T>() where T : class, new()
        {
            List<T> billList = null;

            try
            {
                using (var connection = new SQLiteConnection(Path.Combine(_folderToSaveTo, _databaseName)))
                {
                    billList = connection.Table<T>().ToList();
                }
            }
            catch (SQLiteException)
            {
                //Log ex
            }

            return billList;
        }

        public T Find<T>(Expression<Func<T, bool>> predicate) where T : class, new()
        {
            try
            {
                using (var connection = new SQLiteConnection(Path.Combine(_folderToSaveTo, _databaseName)))
                {
                    return connection.Find(predicate);
                }
            }
            catch (SQLiteException)
            {
                throw new Exception();
            }
        }

        public bool DropTable<T>()
        {
            var success = false;

            try
            {
                using (var connection = new SQLiteConnection(Path.Combine(_folderToSaveTo, _databaseName)))
                {
                    connection.DropTable<T>();
                    success = true;
                }
            }
            catch (SQLiteException)
            {
                //Log ex
            }

            return success;
        }
    }
}
