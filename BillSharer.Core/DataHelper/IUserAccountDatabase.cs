﻿using System;
using System.Collections.Generic;
using BillSharer.Core.Models;

namespace BillSharer.Core.DataHelper
{
    public interface IUserAccountDatabase
    {
        bool CreateDatabase();

        bool DeleteFromTable(UsernamePasswordModel usernamePasswordModel);

        bool InsertIntoTable(UsernamePasswordModel usernamePasswordModel);

        List<UsernamePasswordModel> SelectTable();

        bool SelectQuery(Guid userId);

        bool DropTable();
    }
}
