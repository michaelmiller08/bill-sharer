using BillSharer.Core.DataHelper;
using BillSharer.Core.Repositories;
using BillSharer.Core.Services;
using BillSharer.Core.ViewModels;
using MvvmCross;
using MvvmCross.ViewModels;

namespace BillSharer.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            RegisterInterfaces();

            RegisterAppStart<LoginViewModel>();
        }

        void RegisterInterfaces()
        {
            //Services
            Mvx.IoCProvider.RegisterType<ILoginService, LoginService>();
            Mvx.IoCProvider.RegisterType<IBillService, BillService>();

            //Repositories
            Mvx.IoCProvider.RegisterType<IUserAccountRepository, UserAccountRepository>();
            Mvx.IoCProvider.RegisterType<IBillRepository, BillRepository>();

            //SQlite
            Mvx.IoCProvider.RegisterType<IUserAccountDatabase, UserAccountDatabase>();
            Mvx.IoCProvider.RegisterType<IDatabase, Database>();
        }
    }
}
