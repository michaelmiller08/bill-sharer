﻿using Android.App;
using Android.OS;
using Android.Widget;
using BillSharer.Core.ViewModels;
using MvvmCross.Platforms.Android.Views;

namespace BillSharer.Droid.Activities.Login
{
    [Activity(MainLauncher = true)]
    public class LoginActivity : MvxActivity<LoginViewModel>
    {
        Button _buttonSignIn;
        TextView _usernameField;
        TextView _passwordField;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.activity_login);

            _buttonSignIn = FindViewById<Button>(Resource.Id.ButtonSignIn);
            _usernameField = FindViewById<TextView>(Resource.Id.InputUsername);
            _passwordField = FindViewById<TextView>(Resource.Id.InputPassword);

            SetHints();
        }

        protected override void OnStart()
        {
            base.OnStart();

            _buttonSignIn.Click += ButtonSignIn_Click;
        }

        protected override void OnStop()
        {
            base.OnStop();

            _buttonSignIn.Click -= ButtonSignIn_Click;
        }

        void SetHints()
        {
            _usernameField.Hint = Resources.GetString(Resource.String.username_hint);
            _passwordField.Hint = Resources.GetString(Resource.String.password_hint);
        }

        async void ButtonSignIn_Click(object sender, System.EventArgs e)
        {
            await ViewModel.SignIn();
        }
    }
}
