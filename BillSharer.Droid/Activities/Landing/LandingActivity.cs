﻿using System;
using System.Linq;
using Android.App;
using Android.OS;
using Android.Widget;
using BillSharer.Core.ViewModels;
using MvvmCross.Platforms.Android.Views;

namespace BillSharer.Droid.Activities.Landing
{
    [Activity(Label = "LandingActivity")]
    public class LandingActivity : MvxActivity<LandingViewModel>
    {
        TextView _labelBillsCreated;
        CalendarView _calendar;
        Button _buttonAddBill;
        Button _buttonManageBills;
        
        protected async override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.activity_landing);

            _labelBillsCreated = FindViewById<TextView>(Resource.Id.LabelBillsCreated);
            _calendar = FindViewById<CalendarView>(Resource.Id.Calendar);
            _buttonAddBill = FindViewById<Button>(Resource.Id.ButtonAddBill);
            _buttonManageBills = FindViewById<Button>(Resource.Id.ButtonManageBills);

            await ViewModel.Initialize();

            SetText();
        }

        protected override void OnStart()
        {
            base.OnStart();

            _buttonAddBill.Click += ButtonAddBill_Click;
            _buttonManageBills.Click += ButtonManageBills_Click;

            ViewModel.RefreshBills();
            PopulateOverdueBills();
        }

        protected override void OnStop()
        {
            base.OnStop();

            _buttonAddBill.Click -= ButtonAddBill_Click;
            _buttonManageBills.Click -= ButtonManageBills_Click;
        }

        async void ButtonManageBills_Click(object sender, EventArgs e)
        {
            await ViewModel.NavigateToManageBills();
        }

        void SetText()
        {
            _buttonAddBill.Text = Resources.GetString(Resource.String.add_bill);
            _buttonManageBills.Text = Resources.GetString(Resource.String.manage_bills);
        }

        void PopulateOverdueBills()
        {
            _labelBillsCreated.Text = string.Format(Resources.GetString(Resource.String.bills_due), ViewModel.Bills.Count(b => b.IsOverdue));
        }

        async void ButtonAddBill_Click(object sender, EventArgs e)
        {
            await ViewModel.NavigateToNewBill();
        }
    }
}
