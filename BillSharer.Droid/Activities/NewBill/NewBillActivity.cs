﻿using Android.App;
using Android.OS;
using Android.Widget;
using BillSharer.Core.ViewModels;
using MvvmCross.Platforms.Android.Views;

namespace BillSharer.Droid.Activities.NewBill
{
    [Activity(Label = "NewBillActivity")]
    public class NewBillActivity : MvxActivity<NewBillViewModel>
    {
        Button _buttonCreateBill;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.activity_new_bill);

            _buttonCreateBill = FindViewById<Button>(Resource.Id.ButtonCreateBill);
        }

        protected override void OnStart()
        {
            base.OnStart();

            _buttonCreateBill.Click += ButtonCreateBill_Click;
        }

        protected override void OnStop()
        {
            base.OnStop();

            _buttonCreateBill.Click -= ButtonCreateBill_Click;
        }

        void ButtonCreateBill_Click(object sender, System.EventArgs e)
        {
            ViewModel.AddBill();
        }
    }
}
