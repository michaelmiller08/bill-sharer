using System.ComponentModel;
using Android.App;
using Android.OS;
using Android.Support.V7.Widget;
using BillSharer.Core.ViewModels;
using MvvmCross.Platforms.Android.Views;

namespace BillSharer.Droid.Activities.ManageBills
{
    [Activity(Label = "ManageBillsActivity")]
    public class ManageBillsActivity : MvxActivity<ManageBillsViewModel>
    {
        RecyclerView _recyclerView;
        BillAdapter _listAdapter;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.activity_manage_bills);

            _recyclerView = FindViewById<RecyclerView>(Resource.Id.RecyclerView);
            _recyclerView.HasFixedSize = true;
            _recyclerView.SetLayoutManager(new LinearLayoutManager(this));

            _listAdapter = new BillAdapter(ViewModel.Bills, ViewModel);
            _recyclerView.SetAdapter(_listAdapter);

            RunOnUiThread(async () =>
            {
                await ViewModel.Initialize();
            });
        }

        protected override void OnStart()
        {
            base.OnStart();

            ViewModel.PropertyChanged += ViewModel_PropertyChanged;
        }

        protected override void OnStop()
        {
            base.OnStop();

            ViewModel.PropertyChanged -= ViewModel_PropertyChanged;
        }

        void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ViewModel.Bills))
            {
                RunOnUiThread(UpdateList);
                _listAdapter.NotifyDataSetChanged();
            }
        }

        void UpdateList()
        {
            _listAdapter.UpdateData(ViewModel.Bills);
        }
    }
}
