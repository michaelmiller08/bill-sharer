﻿using System.Collections.Generic;
using System.Linq;
using Android.Support.V7.Widget;
using Android.Views;
using BillSharer.Core.Models;
using BillSharer.Core.ViewModels;

namespace BillSharer.Droid.Activities.ManageBills
{
    public class BillAdapter : RecyclerView.Adapter
    {
        IList<BillModel> _bills;
        readonly ManageBillsViewModel _viewModel;

        public BillAdapter(IEnumerable<BillModel> bills, ManageBillsViewModel viewModel)
        {
            _bills = bills.ToList();
            _viewModel = viewModel;
        }

        public override int ItemCount => _bills.Count;

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var billViewHolder = holder as BillViewHolder;
            if (billViewHolder != null)
            {
                billViewHolder.SetBillName(_bills[position].Name);
                billViewHolder.ItemPosition = position;
            }
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            var itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.bill_item, parent, false);
            return new BillViewHolder(this, itemView);
        }

        public void ItemClicked(int position)
        {
            _viewModel.NavigateToBillDetails(position);
        }

        public void UpdateData(IList<BillModel> bills)
        {
            _bills = bills ?? new List<BillModel>();
            NotifyDataSetChanged();
        }
    }
}
