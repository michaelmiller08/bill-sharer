﻿using System;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;

namespace BillSharer.Droid.Activities.ManageBills
{
    public class BillViewHolder : RecyclerView.ViewHolder
    {
        public int ItemPosition { get; set; }
        
        readonly Button _billName;
        readonly BillAdapter _billAdapter;

        public BillViewHolder(BillAdapter billAdapter, View itemView ) : base(itemView)
        {
            _billAdapter = billAdapter;
            _billName = itemView.FindViewById<Button>(Resource.Id.BillItemHolder);

            _billName.Click += ItemView_Click;
        }

        public void SetBillName(string billName)
        {
            _billName.Text = billName;
        }

        public void ItemView_Click(object sender, EventArgs e)
        {
            _billAdapter.ItemClicked(ItemPosition);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (disposing)
            {
                _billName.Click -= ItemView_Click;
            }
        }
    }
}
