﻿using System;
using Android.App;
using Android.OS;
using Android.Widget;
using BillSharer.Core.ViewModels;
using MvvmCross.Platforms.Android.Views;

namespace BillSharer.Droid.Activities.BillDetails
{
    [Activity(Label = "BillDetailsActivity")]
    public class BillDetailsActivity : MvxActivity<BillDetailsViewModel>
    {
        TextView _labelName;
        TextView _labelDescription;
        Button _buttonDeleteBill;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.activity_bill_details);

            _labelName = FindViewById<TextView>(Resource.Id.LabelBillName);
            _labelDescription = FindViewById<TextView>(Resource.Id.LabelBillDescription);
            _buttonDeleteBill = FindViewById<Button>(Resource.Id.ButtonDeleteBill);

            SetText();
        }

        protected override void OnStart()
        {
            base.OnStart();

            _buttonDeleteBill.Click += Button_Delete_Bill_Click;
        }

        void Button_Delete_Bill_Click(object sender, EventArgs e)
        {
            ViewModel.DeleteCurrentBill();
            OnBackPressed();
        }

        void SetText()
        {
            _labelName.Text = ViewModel.Name;
            _labelDescription.Text = ViewModel.Description;
        }
    }
}
